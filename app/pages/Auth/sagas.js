import { put, takeLatest } from 'redux-saga/effects';

import { apiPostRequest } from 'sagas';
import { openSuccessNotification } from 'containers/NotificationRegistry/actions';
import { LOGOUT, LOGIN, LOGIN_SUCCESS } from './constants';
import { loginSuccess, loginError } from './actions';
import * as API from '../../constants';

// takeEvery(ACTION_NAME_TO_LISTEN_TO_BE_DISPATCHED, generator_function_to_execute_every_time_the_action_is_dispatched);
// takeLatest(ACTION_NAME_TO_LISTEN_TO_BE_DISPATCHED, generator_function_to_execute_on_the_last_time_the_action_is_dispatched);
// select(selector_function_to_be_called_leave_empty_to_get_all_state, ...additional_arguments_to_be_passed);
// call(funtcion_name, function_arguments);
// put(ACTION_NAME_TO_CALL, action_payload);

function* handleLogin({ payload }) {
  const { email, password } = payload;
  const data = {
    identifier: email,
    password,
  };
  yield apiPostRequest(API.LOGIN, loginSuccess, loginError, data);
}

function* handleLoginSuccess() {
  yield put(openSuccessNotification({ title: 'Logged In Successfully' }));
}

function* logout() {
  yield put(openSuccessNotification({ title: 'Logged Out Successfully' }));
}

export function* loginDaemon() {
  yield takeLatest(LOGIN, handleLogin);
}

export function* loginSuccessDaemon() {
  yield takeLatest(LOGIN_SUCCESS, handleLoginSuccess);
}

export function* logoutDaemon() {
  yield takeLatest(LOGOUT, logout);
}
