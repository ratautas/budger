export const OPEN_NOTIFICATION =
  'budger/NotificationRegistry/OPEN_NOTIFICATION';
export const CLOSE_NOTIFICATION =
  'budger/NotificationRegistry/CLOSE_NOTIFICATION';
