export const adaptSebCsv = (raw) =>
  raw
    .filter((row) => row[3] && !isNaN(Number(row[3].replace(',', '.'))))
    .map((row, i) => ({
      id: `import${i}`,
      date: row[1],
      amount: Number(row[3].replace(',', '.')),
      reference: row[4] ?? '',
      meta: `${row[9] ?? ''} ${row[12] ?? ''}`,
    }));

export const adaptRevolutCsv = (raw) =>
  raw
    .filter((row) => row[2] && !isNaN(Number(row[2].replace(',', '.'))))
    .map((row, i) => ({
      id: `import${i}`,
      date: row[0],
      amount: Number(row[2].replace(',', '.')),
      reference: row[1] ?? '',
      meta: row[8] ?? '',
    }));
