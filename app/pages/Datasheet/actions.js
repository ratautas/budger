import {
  GET_DATASHEET,
  GET_DATASHEET_SUCCESS,
  GET_DATASHEET_ERROR,
  UPDATE_ENTRY,
  UPDATE_ENTRY_SUCCESS,
  UPDATE_ENTRY_ERROR,
} from './constants';

export const getDatasheet = payload => ({
  type: GET_DATASHEET,
  payload,
});

export const getDatasheetSuccess = payload => ({
  type: GET_DATASHEET_SUCCESS,
  payload,
});

export const getDatasheetError = payload => ({
  type: GET_DATASHEET_ERROR,
  payload,
});

export const updateEntry = payload => ({
  type: UPDATE_ENTRY,
  payload,
});

export const updateEntrySuccess = payload => ({
  type: UPDATE_ENTRY_SUCCESS,
  payload,
});

export const updateEntryError = payload => ({
  type: UPDATE_ENTRY_ERROR,
  payload,
});
