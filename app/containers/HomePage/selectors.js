/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectUsername = () =>
  createSelector(selectHome, homeState => {
    console.log(homeState.toJS());
    console.log(homeState.get('username'));
    return homeState.get('username');
  });

export { selectHome, makeSelectUsername };
