import { List, fromJS } from 'immutable';

import { OPEN_NOTIFICATION, CLOSE_NOTIFICATION } from './constants';

// unset defaults provided by <ToastNotification>
const defaults = {
  caption: null,
  title: '',
  statusIconDescription: 'status icon',
};

const initialState = List([]);

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case OPEN_NOTIFICATION: {
      return state.push(
        fromJS({
          // id: Date.now(),
          id: Math.floor(Math.random() * 10000),
          ...defaults,
          ...payload,
        }),
      );
    }
    case CLOSE_NOTIFICATION: {
      return state.filter(notification => notification.get('id') !== payload);
    }
    default:
      return state;
  }
}
