import { Map } from 'immutable';

import { SET_APP_LOADING } from './constants';

export default function(state = Map({}), { type, payload }) {
  switch (type) {
    case SET_APP_LOADING: {
      return state.set('isLoading', payload);
    }
    default:
      return state;
  }
}
