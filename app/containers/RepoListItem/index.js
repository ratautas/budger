/**
 * RepoListItem
 *
 * Lists the name and the issue count of a repository
 */

import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectCurrentUser } from 'containers/App/selectors';
import ListItem from 'components/ListItem';

const stateSelector = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

export default function RepoListItem({ item }) {
  const { currentUser } = useSelector(stateSelector);
  let nameprefix = '';

  // If the repository is owned by a different person than we got the data for
  // it's a fork and we should show the name of the owner
  if (item.owner.login !== currentUser) {
    nameprefix = `${item.owner.login}/`;
  }

  // Put together the content of the repository
  const content = (
    <div>
      <a href={item.html_url} target="_blank">
        {nameprefix + item.name}
      </a>
      <a href={`${item.html_url}/issues`} target="_blank">
        {item.open_issues_count}
      </a>
    </div>
  );

  // Render the content into a list item
  return <ListItem key={`repo-list-item-${item.full_name}`} item={content} />;
}

RepoListItem.propTypes = {
  item: PropTypes.object,
};
