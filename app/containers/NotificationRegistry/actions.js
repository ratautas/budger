import { OPEN_NOTIFICATION, CLOSE_NOTIFICATION } from './constants';

export const openNotification = payload => ({
  type: OPEN_NOTIFICATION,
  payload,
});

export const openSuccessNotification = payload => ({
  type: OPEN_NOTIFICATION,
  payload: { ...payload, kind: 'success' },
});

export const openErrorNotification = openNotification;

export const closeNotification = payload => ({
  type: CLOSE_NOTIFICATION,
  payload,
});
