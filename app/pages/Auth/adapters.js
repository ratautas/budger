// because API returns two sepatate objects (user and jwt),
// this micro adapter merges them into one
export function mergeAuthToken(authPayload) {
  const { auth, jwt } = authPayload;
  return { ...auth, token: jwt };
}
