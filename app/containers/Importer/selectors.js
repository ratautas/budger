import { createSelector } from 'reselect';

export const selectImporter = state => state.get('importer');

export const selectNotificationIsActive = createSelector(
  selectImporter,
  importer => importer.get('isActive'),
);

export const selectNotificationParams = createSelector(
  selectImporter,
  importer => importer.get('params'),
);
