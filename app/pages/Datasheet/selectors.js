import { Map, List } from 'immutable';
import { createSelector } from 'reselect';

export const selectDatasheet = (state) => state.get('datasheet') ?? Map({});

export const selectEntries = createSelector(
  selectDatasheet,
  (datasheet) => datasheet.get('entries') ?? List([]),
);

export const selectCategories = createSelector(
  selectDatasheet,
  (datasheet) => datasheet.get('categories') ?? List([]),
);

export const selectTags = createSelector(
  selectDatasheet,
  (datasheet) => datasheet.get('tags') ?? List([]),
);

export const selectDatasheetCategories = createSelector(
  selectCategories,
  (categories) =>
    [...categories].map((category) => ({
      id: category.get('id'),
      title: category.get('title'),
    })),
);

export const selectEntryRows = createSelector(
  selectEntries,
  selectDatasheetCategories,
  (entries, categories) =>
    [...entries].map((entry) => {
      const catId = entry.get('category') ?? 0;
      const category = categories.find((cat) => cat.id === catId);
      return {
        category,
        id: entry.get('id').toString(),
        key: entry.get('id').toString(),
        amount: entry.get('amount'),
        reference: entry.get('reference'),
        meta: entry.get('meta'),
        date: entry.get('date'),
      };
    }),
);
