import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { actions as authActions, selectors as authSelectors } from 'pages/Auth';
import { selectAppLoading } from './selectors';
import { setAppLoading } from './actions';
import App from './App';
import './index.scss';

const mapStateToProps = createStructuredSelector({
  // ...authSelectors,
  authLoggedIn: authSelectors.selectAuthLoggedIn,
  appLoading: selectAppLoading,
});

const mapDispatchToProps = {
  // ...authActions,
  logout: authActions.logout,
  setAppLoading,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(App);
