import { fromJS } from 'immutable';

import { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT } from './constants';
import { adaptResponseErrors } from '../../adapters';

const initialState = fromJS({
  user: {},
  token: '',
  loading: false,
  error: {},
  loggedIn: false,
});

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case LOGIN: {
      return state.merge(initialState).set('loading', true);
    }

    case LOGIN_SUCCESS: {
      return state
        .merge(initialState)
        .set('user', fromJS(payload.user))
        .set('token', payload.jwt)
        .set('loggedIn', true);
    }

    case LOGIN_ERROR: {
      return state
        .merge(initialState)
        .set('error', fromJS(adaptResponseErrors(payload)));
    }
    case LOGOUT: {
      return initialState;
    }

    default:
      return state;
  }
}
