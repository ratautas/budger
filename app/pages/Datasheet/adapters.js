export function adaptKeysForDataTable(entriesPayload) {
  return entriesPayload.map((entry) => {
    const id = entry.id.toString();
    return { ...entry, id, key: id };
  });
}

export function adaptDatasheet(entriesPayload) {
  const adaptedPayload = { ...entriesPayload };
  const { categories, entries, tags } = entriesPayload;
  console.log({ entriesPayload });
  if (categories && categories.length) {
    adaptedPayload.categories = adaptKeysForDataTable(categories);
  }
  if (entries && entries.length) {
    adaptedPayload.entries = adaptKeysForDataTable(categories);
  }
  if (tags && tags.length) {
    adaptedPayload.tags = adaptKeysForDataTable(categories);
  }
  return adaptedPayload;
}

export function adaptEntrySuccess(entry) {
  return { ...entry, category: entry.category.id };
}
