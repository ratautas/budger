import { compose } from 'redux';
import { connect } from 'react-redux';
import { injectReducer } from 'redux-injectors';
import { createStructuredSelector } from 'reselect';

import reducer from './reducers';
import * as actions from './actions';
import * as selectors from './selectors';
import NotificationRegistry from './NotificationRegistry';
import './index.scss';

const mapStateToProps = createStructuredSelector({
  // ...selectors,
  notificationRegistry: selectors.selectNotificationRegistry,
});

const mapDispatchToProps = {
  ...actions,
};

export { actions, selectors };

export default compose(
  injectReducer({ key: 'notificationRegistry', reducer }),
  connect(mapStateToProps, mapDispatchToProps),
)(NotificationRegistry);
