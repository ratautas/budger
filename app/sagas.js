import { all, put, select, call } from 'redux-saga/effects';

import request from 'utils/request';
import { selectAuthToken } from 'pages/Auth/selectors';
import { setAppLoading } from 'containers/App/actions';
import { openErrorNotification } from 'containers/NotificationRegistry/actions';
import { adaptResponseErrors } from './adapters';

export function* apiRequest(url, onSuccess, onError, data, method) {
  yield put(setAppLoading(true));
  try {
    const fetchParams = { headers: { 'Content-Type': 'application/json' } };
    const token = yield select(selectAuthToken);
    if (token) fetchParams.headers.Authorization = `Bearer ${token}`;
    if (data) fetchParams.body = JSON.stringify(data);
    if (method) fetchParams.method = method;
    const response = yield call(request, url, { ...fetchParams });
    if (!response.error) {
      yield put(onSuccess(response));
    } else if (onError) {
      const { messages } = yield call(adaptResponseErrors, response);
      yield all(
        [...messages].map(message =>
          put(openErrorNotification({ title: message.message })),
        ),
      );
      yield put(onError(response));
    }
    yield put(setAppLoading(false));
    return response;
  } catch (error) {
    if (onError) {
      yield put(onError(error));
      yield put(openErrorNotification({ title: 'Somethings Wrong with API' }));
    }
    yield put(setAppLoading(false));
    return error;
  }
}

export function* apiGetRequest(url, onSuccess, onError) {
  return yield apiRequest(url, onSuccess, onError);
}

export function* apiPostRequest(url, onSuccess, onError, data) {
  return yield apiRequest(url, onSuccess, onError, data, 'POST');
}

export function* apiUpdateRequest(url, onSuccess, onError, data) {
  return yield apiRequest(url, onSuccess, onError, data, 'PUT');
}
