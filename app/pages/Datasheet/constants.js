export const GET_DATASHEET = 'budger/Datasheet/GET_DATASHEET';
export const GET_DATASHEET_SUCCESS = 'budger/Datasheet/GET_DATASHEET_SUCCESS';
export const GET_DATASHEET_ERROR = 'budger/Datasheet/GET_DATASHEET_ERROR';

export const UPDATE_ENTRY = 'budger/Datasheet/UPDATE_ENTRY';
export const UPDATE_ENTRY_SUCCESS = 'budger/Datasheet/UPDATE_ENTRY_SUCCESS';
export const UPDATE_ENTRY_ERROR = 'budger/Datasheet/UPDATE_ENTRY_ERROR';
