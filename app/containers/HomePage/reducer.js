import { fromJS } from 'immutable';

import { CHANGE_AUTHNAME } from './constants';

export const initialState = fromJS({ username: '' });

export default (state = initialState, { type, username }) => {
  switch (type) {
    case CHANGE_AUTHNAME: {
      // console.log(state.toJS());
      // console.log(username);
      console.log(fromJS({ username: username.replace(/@/gi, '') }));
      return state.merge(fromJS({ username: username.replace(/@/gi, '') }));
    }
    default: {
      return state;
    }
  }
};
