import { compose } from 'redux';
import { connect } from 'react-redux';
import { injectReducer, injectSaga } from 'redux-injectors';
import { createStructuredSelector } from 'reselect';

import {
  getDatasheetDaemon,
  getDatasheetSuccessDaemon,
  updateEntryDaemon,
  updateEntrySuccessDaemon,
} from './sagas';
import reducer from './reducers';
import * as actions from './actions';
import * as selectors from './selectors';
import Datasheet from './Datasheet';
import './index.scss';

const mapStateToProps = createStructuredSelector({
  ...selectors,
  entries: selectors.selectEntries,
  entryRows: selectors.selectEntryRows,
  datasheetCategories: selectors.selectDatasheetCategories,
});

const mapDispatchToProps = {
  ...actions,
};

export { actions, selectors };

export default compose(
  injectReducer({ key: 'datasheet', reducer }),
  injectSaga({ key: 'getDatasheet', saga: getDatasheetDaemon }),
  injectSaga({ key: 'getDatasheetSuccess', saga: getDatasheetSuccessDaemon }),
  injectSaga({ key: 'updateEntry', saga: updateEntryDaemon }),
  injectSaga({ key: 'updateEntrySuccess', saga: updateEntrySuccessDaemon }),
  connect(mapStateToProps, mapDispatchToProps),
)(Datasheet);
