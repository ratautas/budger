import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { ToastNotification } from 'carbon-components-react';
class NotificationRegistry extends Component {
  componentDidUpdate(prevProps) {
    const { notificationRegistry } = this.props;
    if (prevProps.notificationRegistry.size < notificationRegistry.size) {
      const latestNotification = notificationRegistry.last();
      const latestNotificationID = latestNotification.get('id');
      const latestNotificationDelay = latestNotification.get('delay') || 5000;
      setTimeout(
        () => this.props.closeNotification(latestNotificationID),
        latestNotificationDelay,
      );
    }
  }

  render() {
    const { notificationRegistry } = this.props;
    return notificationRegistry.size ? (
      <div className="notificationRegistry">
        {notificationRegistry.map(notification => {
          const id = notification.get('id');
          return (
            <ToastNotification
              className="notificationRegistry__toast"
              statusIconDescription=""
              key={id}
              onCloseButtonClick={() => this.props.closeNotification(id)}
              title={notification.get('title')}
              caption={notification.get('caption')}
              kind={notification.get('kind')}
            />
          );
        })}
      </div>
    ) : null;
  }
}

NotificationRegistry.defaultProps = {
  notificationRegistry: null,
  closeNotification: () => ({}),
};

NotificationRegistry.propTypes = {
  notificationRegistry: ImmutablePropTypes.list,
  closeNotification: PropTypes.func,
};

export default NotificationRegistry;
