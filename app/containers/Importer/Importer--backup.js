import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Control, LocalForm } from 'react-redux-form';
import Papa from 'papaparse';
import {
  Button,
  Select,
  SelectItem,
  FileUploaderButton,
  FileUploaderDropContainer,
} from 'carbon-components-react';

// import { submitImport } from './actions';
import { BANKS } from './constants';
import { adaptSebCsv, adaptRevolutCsv } from './adapters';

const DropArea = props => (
  <FileUploaderDropContainer
    accept={['.csv']}
    labelText="Drag and drop files here or click to upload"
    multiple={false}
    name=""
    // onAddFiles={function noRefCheck() { }}
    // onChange={function noRefCheck() { }}
    role=""
    tabIndex={0}
  />
);

const MyComp = props => (
  <FileUploaderButton
    accept={['.csv']}
    buttonKind="primary"
    className="bob"
    disableLabelChanges={false}
    labelText="Add files"
    name=""
    multiple={false}
    // onChange={function noRefCheck() { }}
    // onClick={function noRefCheck() { }}
    role="button"
    tabIndex={0}
  // {...props}
  />
);

class Importer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bank: BANKS[0].value,
    };
    this.attachDispatch = this.attachDispatch.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleAddFiles = this.handleAddFiles.bind(this);
    this.hangleFilesChange = this.hangleFilesChange.bind(this);
    this.handleFormUpdate = this.handleFormUpdate.bind(this);
    this.handeDropAreaChange = this.handeDropAreaChange.bind(this);
  }

  handeDropAreaChange(e) {
    console.log({ e });
  }

  handleAddFiles(e, { addedFiles }) {
    console.log({ addedFiles });
    addedFiles.map(file => {
      console.log(file);
    });
  }

  hangleFilesChange(files) {
    // console.log({ files })
    // console.log('hangleFilesChange')
  }

  attachDispatch(dispatch) {
    this.formDispatch = dispatch;
  }

  adaptBankData(data) {
    switch (this.state.bank) {
      case BANKS[0].value:
        return adaptSebCsv(data); // SEB
      case BANKS[1].value:
        return adaptRevolutCsv(data); // REVOLUT
      default:
        return 'error'; // TODO - add error handling
    }
  }

  getFileData(data) {
    try {
      return Papa.parse(data.files[0], {
        complete: results => {
          const bankData = this.adaptBankData(
            results.data,
          ).map((result, i) => ({ ...result, id: `${i}` }));
          console.log(bankData);
          this.setState({ preview: bankData });
        },
        error: error => {
          console.log(error); // TODO - add error handling
        },
      });
    } catch (error) {
      console.log(error); // TODO - add error handling
      return error;
    }
  }

  handleFormSubmit({ data }) {
    console.log(data);
    this.getFileData({ data });
  }

  handleFormUpdate(values) {
    console.log({ values });
  }

  handleFormChange(model) {
    console.log({ model });
    this.setState(model);
  }

  render() {
    return (
      <>
        {this.state.bank}
        <LocalForm
          onSubmit={this.handleFormSubmit}
          onChange={this.handleFormChange}
          onUpdate={this.handleFormUpdate}
          getDispatch={this.attachDispatch}
          model="import"
          initialState={{
            bank: BANKS[0].value,
            files: [],
            preview: [],
          }}
        >
          <Control.select
            model="import.bank"
            id="import.bank"
            component={Select}
          >
            {BANKS.map(bank => (
              <SelectItem
                key={bank.value}
                text={bank.label}
                value={bank.value}
              />
            ))}
          </Control.select>
          <Control.file
            model="import.files"
            component={MyComp}
            mapProps={{
              invalid: field =>
                !(field?.fieldValue?.valid ?? true) &&
                (field?.fieldValue?.touched ?? false),
              invalidText: field =>
                field?.fieldValue?.errors?.hasValue ? 'is required' : false,
            }}
            validateOn="change"
            validators={{ hasValue: val => val && val.length }}
          />
          <Control.file
            model="import.files2"
            component={DropArea}
            mapProps={{
              invalid: field =>
                !(field?.fieldValue?.valid ?? true) &&
                (field?.fieldValue?.touched ?? false),
              invalidText: field =>
                field?.fieldValue?.errors?.hasValue ? 'is required' : false,
            }}
            validateOn="change"
            validators={{ hasValue: val => val && val.length }}
          />
          <Button type="submit">Upload</Button>
        </LocalForm>
      </>
    );
  }
}

Importer.defaultProps = {
  importer: null,
  closeNotification: () => ({}),
};

Importer.propTypes = {
  importer: ImmutablePropTypes.list,
  closeNotification: PropTypes.func,
};

export default Importer;
