/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet-async';

import List from './List';
import ListItem from './ListItem';
import ListItemTitle from './ListItemTitle';

export default function FeaturePage() {
  return (
    <div>
      <Helmet>
        <title>Feature Page</title>
        <meta
          name="description"
          content="Feature page of React Boilerplate application"
        />
      </Helmet>

      <List>
        <ListItem>
          <ListItemTitle>scaffoldingHeader</ListItemTitle>
          <p>scaffoldingMessage</p>
        </ListItem>

        <ListItem>
          <ListItemTitle>feedbackHeader</ListItemTitle>
          <p>feedbackMessage</p>
        </ListItem>

        <ListItem>
          <ListItemTitle>routingHeader</ListItemTitle>
          <p>routingMessage</p>
        </ListItem>

        <ListItem>
          <ListItemTitle>networkHeader</ListItemTitle>
          <p>networkMessage</p>
        </ListItem>

        <ListItem>
          <ListItemTitle>intlHeader</ListItemTitle>
          <p>intlMessage</p>
        </ListItem>
      </List>
    </div>
  );
}
