import React from 'react';
import PropTypes from 'prop-types';
import { Button, DataTable, Dropdown } from 'carbon-components-react';

export const entryHeaders = [
  { header: 'Amount', key: 'amount' },
  { header: 'Date', key: 'date' },
  { header: 'Category', key: 'category' },
  { header: 'Reference', key: 'reference' },
  // { header: 'Meta', key: 'meta' },
];

// https://github.com/carbon-design-system/carbon-components-react/tree/master/src/components/DataTable/

const {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  TableHeader,
  TableToolbar,
  TableBatchActions,
  TableBatchAction,
  TableToolbarSearch,
  TableToolbarContent,
  TableToolbarAction,
  TableSelectAll,
  TableSelectRow,
  TableExpandHeader,
  TableExpandRow,
  TableExpandedRow,
} = DataTable;

const Datasheet = (props) => {
  const { tableRows, tableCategories, onCategoryChange, title } = props;
  const batchActionClick = (selectedRows) => {
    console.log({ selectedRows });
  };
  const action = (payload) => () => {
    console.log({ payload });
  };
  return (
    <DataTable
      headers={entryHeaders}
      rows={tableRows}
      render={({
        rows,
        headers,
        getHeaderProps,
        getRowProps,
        getTableProps,
        getSelectionProps,
        getBatchActionProps,
        onInputChange,
        /* the selected rows are provided as a render prop */
        selectedRows,
      }) => (
        <TableContainer title={title}>
          <TableToolbar>
            {/* make sure to apply getBatchActionProps so that the bar renders */}
            <TableBatchActions {...getBatchActionProps()}>
              {/* inside of you batch actinos, you can include selectedRows */}
              <TableBatchAction onClick={batchActionClick(selectedRows)}>
                Ghost
              </TableBatchAction>
              <TableBatchAction onClick={batchActionClick(selectedRows)}>
                Ghost
              </TableBatchAction>
              <TableBatchAction onClick={batchActionClick(selectedRows)}>
                Ghost
              </TableBatchAction>
            </TableBatchActions>
            <TableToolbarSearch onChange={onInputChange} />
            <TableToolbarContent>
              <Button
                onClick={action('Add new row')}
                size="small"
                kind="primary"
              >
                Add new
              </Button>
            </TableToolbarContent>
          </TableToolbar>
          <Table>
            <TableHead>
              <TableRow>
                <TableExpandHeader />
                <TableSelectAll {...getSelectionProps()} />
                {headers.map((header) => (
                  <TableHeader {...getHeaderProps({ header })}>
                    {header.header}
                  </TableHeader>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                // <TableRow key={row.id}>
                <React.Fragment key={row.id}>
                  <TableExpandRow key={row.id} {...getRowProps({ row })}>
                    <TableSelectRow {...getSelectionProps({ row })} />
                    {row.cells.map((cell) => {
                      const {
                        id,
                        info: { header },
                        value,
                      } = cell;
                      return (
                        <TableCell key={id}>
                          {header !== 'category' && header !== 'tags' && (
                            <div>{value}</div>
                          )}
                          {tableCategories.length > 0 &&
                            header === 'category' && (
                            <Dropdown
                                id={id}
                                itemToString={(item) =>
                                  item ? item.title : ''
                                }
                              items={tableCategories}
                                selectedItem={value}
                              onChange={({ selectedItem }) =>
                                onCategoryChange(row.id, selectedItem.id)
                                }
                              label="Dropdown menu options"
                              />
                          )}
                        </TableCell>
                      );
                    })}
                  </TableExpandRow>
                  {row.isExpanded && (
                    <TableExpandedRow colSpan={headers.length + 2}>
                      {row.cells[row.cells.length - 1].value}
                    </TableExpandedRow>
                  )}
                </React.Fragment>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    />
  );
};

Datasheet.defaultProps = {
  title: null,
  tableCategories: [],
  onCategoryChange: () => ({}),
};

Datasheet.propTypes = {
  tableRows: PropTypes.array.isRequired,
  title: PropTypes.string,
  tableCategories: PropTypes.array,
  onCategoryChange: PropTypes.func,
};

export default Datasheet;
