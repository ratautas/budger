import { compose } from 'redux';
import { connect } from 'react-redux';
import { injectReducer, injectSaga } from 'redux-injectors';
import { createStructuredSelector } from 'reselect';

import { loginDaemon, loginSuccessDaemon, logoutDaemon } from './sagas';
import reducer from './reducers';
import * as actions from './actions';
import * as selectors from './selectors';
import Auth from './Auth';
import './index.scss';

const mapStateToProps = createStructuredSelector({
  // ...selectors,
  userName: selectors.selectUserName,
});

const mapDispatchToProps = {
  // ...actions,
  login: actions.login,
};

export { actions, selectors };

export default compose(
  injectReducer({ key: 'auth', reducer }),
  injectSaga({ key: 'login', saga: loginDaemon }),
  injectSaga({ key: 'loginSuccess', saga: loginSuccessDaemon }),
  injectSaga({ key: 'logout', saga: logoutDaemon }),
  connect(mapStateToProps, mapDispatchToProps),
)(Auth);
