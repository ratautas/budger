1. Padaryt importavimą + check for duplicates
2. Pridėti tag'us
3. Kategorijų pridėjimas/redagavimas
4. Kategorijų remappinimas pagal 'meta'
4. Invite collaboratores
5. Išvalyt legacy komponentus

9. Persist data/store su localstorage
10. Importer.js padaryt, kad rodytų lentelę su duplikuotais
99. Pasirašyt testus
999. Multilang :O

* normaliai implementint konteinerių struktūrą, kur konteineriai tik passina propsus, o renderina komponentai
* implementint index + ComponentName patternas
* panaudot \_error ir \_success patterną reduceriui / sagom;
* padaryt router su redux;
* padaryt redux su adapters;
* pridėt propTypes;

- padaryt redux su sagom
- autorizacija su JWT;
- padaryt redux su 'immutable';
- padaryt redux su reselect;
- padaryt formą redux;

- idėt normaliai persist store;
- error handling

- parašyt testą/kitą;
- pridėt kalbas iš react boilerplate;

- deploy via SSH with GITLAB
- perdaryt toasts į queue

https://auth0.com/blog/beyond-create-react-app-react-router-redux-saga-and-more/
https://github.com/bfillmer/saga-restart
https://sendgrid.com/blog/using-the-adapter-design-pattern-with-react/

Home:

- User
- Entries
- Categories
- Locations
- Import

Tables:

- users
- categories
- locations
- sources (Revolut, SEB)
- entries/collections
- mappings? - primappina išrašo laukelį prie kategorijos/lokacijos, kad būtų galima automatiškai priskirt, ką į ką paverst, panašiai kaip translationai

PIVOT sujungti entrius su keliais useriais
https://www.youtube.com/watch?v=eyOvM20ho-o

SelectDatasheet (Modal?) - parodyti collaboratorius, gal net paskutinius entrius
NewDatasheet
**Collaborators
**Categories
**Tags
**Entries (Import)
Datasheet (Lentelė filtravimui + Import)
NewEntry
