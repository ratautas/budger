import React from 'react';
import { NavLink } from 'react-router-dom';

import Banner from './banner.jpg';

function Header() {
  return (
    <div>
      <a href="https://www.reactboilerplate.com/">
        <img src={Banner} alt="react-boilerplate - Logo" />
      </a>
      <div>
        <NavLink to="/">home</NavLink>
        <NavLink to="/features">features</NavLink>
        <NavLink to="/testy">testy</NavLink>
      </div>
    </div>
  );
}

export default Header;
