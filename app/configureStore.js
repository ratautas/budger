import { createStore, applyMiddleware, compose } from 'redux';
import { createInjectorsEnhancer, forceReducerReload } from 'redux-injectors';
import createSagaMiddleware from 'redux-saga';
import { Map } from 'immutable';
import { combineReducers } from 'redux-immutable';
import {
  connectRouter,
  routerMiddleware,
} from 'connected-react-router/immutable';
import { createBrowserHistory } from 'history';

import appReducer from 'containers/App/reducer';
// import appReducer from './reducer.js';

const initialStateMap = Map({});

export const history = createBrowserHistory();

export function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    app: appReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return rootReducer;
}

// export default function configureStore(initialState = Map({}), historyState) {
export default function configureStore(historyState) {
  let composeEnhancers = compose;
  const reduxSagaMonitorOptions = {};

  // If Redux Dev Tools and Saga Dev Tools Extensions are installed, enable them
  /* istanbul ignore next */
  if (process.env.NODE_ENV !== 'production' && typeof window === 'object') {
    /* eslint-disable no-underscore-dangle */
    if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});
    }
    /* eslint-enable */
  }

  const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);
  const { run: runSaga } = sagaMiddleware;

  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const middlewares = [sagaMiddleware, routerMiddleware(historyState)];

  const enhancers = [
    applyMiddleware(...middlewares),
    createInjectorsEnhancer({
      createReducer,
      runSaga,
    }),
  ];

  const store = createStore(
    createReducer(),
    initialStateMap,
    composeEnhancers(...enhancers),
  );

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      forceReducerReload(store);
    });
  }

  return store;
}
