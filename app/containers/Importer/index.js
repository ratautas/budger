import { compose } from 'redux';
import { connect } from 'react-redux';
import { injectReducer } from 'redux-injectors';
import { createStructuredSelector } from 'reselect';

import { selectEntries } from 'pages/Datasheet/selectors';
import reducer from './reducers';
import * as actions from './actions';
import * as selectors from './selectors';
import Importer from './Importer';
import './index.scss';

const mapStateToProps = createStructuredSelector({
  // ...selectors,
  importer: selectors.selectImporter,
  entries: selectEntries,
});

const mapDispatchToProps = {
  ...actions,
};

export { actions, selectors };

export default compose(
  injectReducer({ key: 'importer', reducer }),
  connect(mapStateToProps, mapDispatchToProps),
)(Importer);
