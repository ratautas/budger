import { createSelector } from 'reselect';

export const selectApp = state => state.get('app');

export const selectAppLoading = createSelector([selectApp], app =>
  app.get('isLoading'),
);
