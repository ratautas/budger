import { SET_APP_LOADING } from './constants';

export const setAppLoading = payload => ({ type: SET_APP_LOADING, payload });
