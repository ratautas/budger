import { put, select, takeLatest } from 'redux-saga/effects';

import { apiGetRequest, apiUpdateRequest } from 'sagas';
import { selectDefaultDatasheetID } from 'pages/Auth/selectors';
import { openSuccessNotification } from 'containers/NotificationRegistry/actions';
import {
  GET_DATASHEET,
  GET_DATASHEET_SUCCESS,
  UPDATE_ENTRY,
  UPDATE_ENTRY_SUCCESS,
} from './constants';
import {
  getDatasheetSuccess,
  getDatasheetError,
  updateEntrySuccess,
  updateEntryError,
} from './actions';
import * as API from '../../constants';

function* handleGetDatasheet() {
  const defaultDatasheetID = yield select(selectDefaultDatasheetID);
  yield apiGetRequest(
    `${API.DATASHEETS}/${defaultDatasheetID}`,
    getDatasheetSuccess,
    getDatasheetError,
  );
}

function* handleGetDatasheetSuccess() {
  yield put(
    openSuccessNotification({ title: 'Default Datasheet Loaded Successfully' }),
  );
}

function* handleUpdateEntry({ payload }) {
  const { entryID, update } = payload;
  yield apiUpdateRequest(
    `${API.ENTRIES}/${entryID}`,
    updateEntrySuccess,
    updateEntryError,
    update,
  );
}

function* handleUpdateEntrySuccess() {
  yield put(openSuccessNotification({ title: 'Entry Updated Successfully' }));
}

export function* getDatasheetDaemon() {
  yield takeLatest(GET_DATASHEET, handleGetDatasheet);
}

export function* getDatasheetSuccessDaemon() {
  yield takeLatest(GET_DATASHEET_SUCCESS, handleGetDatasheetSuccess);
}

export function* updateEntryDaemon() {
  yield takeLatest(UPDATE_ENTRY, handleUpdateEntry);
}

export function* updateEntrySuccessDaemon() {
  yield takeLatest(UPDATE_ENTRY_SUCCESS, handleUpdateEntrySuccess);
}
