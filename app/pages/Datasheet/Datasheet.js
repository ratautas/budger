/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import DatasheetTable from 'components/DatasheetTable';
import Importer from 'containers/Importer';

class Datasheet extends Component {
  constructor() {
    super();
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
  }

  componentDidMount() {
    this.props.getDatasheet();
  }

  handleCategoryChange(entryID, categoryID) {
    this.props.updateEntry({
      entryID,
      update: { category: categoryID },
    });
  }

  render() {
    const { entryRows, datasheetCategories } = this.props;
    return (
      <>
        <Importer />
        {entryRows.length && (
          <DatasheetTable
            tableRows={entryRows}
            tableCategories={datasheetCategories}
            onCategoryChange={this.handleCategoryChange}
          />
        )}
      </>
    );
  }
}

Datasheet.defaultProps = {
  entryRows: [],
  datasheetCategories: [],
  getDatasheet: () => ({}),
  updateEntry: () => ({}),
};

Datasheet.propTypes = {
  entryRows: PropTypes.array,
  datasheetCategories: PropTypes.array,
  getDatasheet: PropTypes.func,
  updateEntry: PropTypes.func,
};

export default Datasheet;
