import React, { Component } from 'react';
import { List } from 'immutable';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Control, LocalForm, actions } from 'react-redux-form';
import Papa from 'papaparse';
import {
  Select,
  SelectItem,
  FileUploaderDropContainer,
} from 'carbon-components-react';

import DatasheetTable from 'components/DatasheetTable';
import { BANKS } from './constants';
import { adaptSebCsv, adaptRevolutCsv } from './adapters';

class Importer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      preview: null,
      data: {},
      model: {
        bank: BANKS[0].value,
        csv: [],
      },
    };
    this.attachDispatch = this.attachDispatch.bind(this);
    this.handleFormChange = this.handleFormChange.bind(this);
    this.onAddFiles = this.onAddFiles.bind(this);
    this.getFileData = this.getFileData.bind(this);
  }

  onAddFiles(e, { addedFiles }) {
    this.formDispatch(actions.change('import.csv', addedFiles));
    if (addedFiles[0]) this.getFileData(addedFiles[0]);
  }

  attachDispatch(dispatch) {
    this.formDispatch = dispatch;
  }

  adaptBankData(data) {
    switch (this.state.model.bank) {
      case BANKS[0].value: // SEB
        return adaptSebCsv(data);
      case BANKS[1].value: // REVOLUT
        return adaptRevolutCsv(data);
      default:
        return 'error'; // TODO - add error handling
    }
  }

  async getFileData(file) {
    try {
      return Papa.parse(file, {
        complete: (results) => {
          const preview = this.adaptBankData(results.data)
            .filter(
              (importedEntry) =>
                !this.props.entries.find(
                  (storedEntry) =>
                    importedEntry.date === storedEntry.get('date') &&
                    importedEntry.amount === storedEntry.get('amount') &&
                    importedEntry.reference === storedEntry.get('reference'),
                ),
            )
            .map((row) => ({
              ...row,
              // disabled: true,
              isSelected: true,
              isExpanded: true,
            }));
          this.setState({ preview });
        },
        error: (error) => {
          console.log(error); // TODO - add error handling
        },
      });
    } catch (error) {
      console.log(error); // TODO - add error handling
      return error;
    }
  }

  handleFormChange(model) {
    this.setState({ model });
  }

  render() {
    return (
      <>
        {this.state.bank}
        <LocalForm
          onSubmit={this.handleFormSubmit}
          onChange={this.handleFormChange}
          onUpdate={this.handleFormUpdate}
          getDispatch={this.attachDispatch}
          model="import"
          initialState={{ bank: BANKS[0].value }}
        >
          <Control.select
            model="import.bank"
            id="import.bank"
            component={Select}
          >
            {BANKS.map((bank) => (
              <SelectItem
                key={bank.value}
                text={bank.label}
                value={bank.value}
              />
            ))}
          </Control.select>
          <Control.file
            model="import.csv"
            id="import.csv"
            component={() => (
              <FileUploaderDropContainer onAddFiles={this.onAddFiles} />
            )}
          />
        </LocalForm>
        {this.state.preview && (
          <DatasheetTable tableRows={this.state.preview} />
        )}
      </>
    );
  }
}

Importer.defaultProps = {
  entries: List([]),
  closeNotification: () => ({}),
};

// PropTypes.oneOfType([PropTypes.object]).isRequired
Importer.propTypes = {
  entries: ImmutablePropTypes.list,
  closeNotification: PropTypes.func,
};

export default Importer;
