export const LOGIN = 'budger/Auth/LOGIN';
export const LOGIN_SUCCESS = 'budger/Auth/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'budger/Auth/LOGIN_ERROR';

export const LOGOUT = 'budger/Auth/LOGOUT';

export const REGISTRATION_SUBMIT = 'budger/Auth/REGISTRATION_SUBMIT';
export const REGISTRATION_SUCCESS = 'budger/Auth/REGISTRATION_SUCCESS';
export const REGISTRATION_ERROR = 'budger/Auth/REGISTRATION_ERROR';
