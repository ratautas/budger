import { fromJS } from 'immutable';

import { adaptResponseErrors } from 'adapters';
import {
  GET_DATASHEET,
  GET_DATASHEET_SUCCESS,
  GET_DATASHEET_ERROR,
  UPDATE_ENTRY,
  UPDATE_ENTRY_SUCCESS,
} from './constants';
import { adaptEntrySuccess } from './adapters';

const initialState = fromJS({
  loading: false,
  error: {},
});

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case GET_DATASHEET: {
      return state.merge(initialState).set('loading', true);
    }

    case GET_DATASHEET_SUCCESS: {
      console.log({ payload });
      return state.merge(fromJS(payload)).set('loading', false);
    }

    case GET_DATASHEET_ERROR: {
      return state
        .merge(initialState)
        .set('error', fromJS(adaptResponseErrors(payload)));
    }

    case UPDATE_ENTRY: {
      return state.set('loading', true);
    }

    case UPDATE_ENTRY_SUCCESS: {
      const entryIndex = state
        .get('entries')
        .findIndex((entry) => entry.get('id') === payload.id);
      return state.setIn(
        ['entries', entryIndex],
        fromJS(adaptEntrySuccess(payload)),
      );
    }

    default:
      return state;
  }
}
