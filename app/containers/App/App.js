/* eslint-disable react/prefer-stateless-function */
import React, { Component, lazy, Suspense } from 'react';
import { Helmet } from 'react-helmet-async';
import { Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import { Loading } from 'carbon-components-react';

const Auth = lazy(() => import('pages/Auth'));
const Dashboard = lazy(() => import('pages/Dashboard'));
const Datasheet = lazy(() => import('pages/Datasheet'));
const Notfound = lazy(() => import('pages/Notfound'));

const NotificationRegistry = lazy(() =>
  import('containers/NotificationRegistry'),
);

class App extends Component {
  render() {
    const { authLoggedIn, appLoading, logout } = this.props;
    return (
      <Suspense fallback={<div>loading...</div>}>
        <Helmet
          titleTemplate="%s - React Boilerplate"
          defaultTitle="React Boilerplate"
        >
          <meta name="description" content="A React Boilerplate application" />
        </Helmet>
        {/* <AppHeader /> */}
        <Content className="bx--grid">
          <button type="button" onClick={logout}>
            klik
          </button>
          <Switch>
            {authLoggedIn ? (
              <Route exact path="/" component={Datasheet} />
            ) : (
              <Route component={Auth} />
            )}
            <Route component={Notfound} />
          </Switch>
        </Content>
        <NotificationRegistry />
        {appLoading && <Loading description="Active loading indicator" />}
      </Suspense>
    );
  }
}

App.propTypes = {
  authLoggedIn: PropTypes.bool,
  appLoading: PropTypes.bool,
  setAppLoading: PropTypes.func,
};

App.defaultProps = {
  authLoggedIn: false,
  appLoading: false,
  setAppLoading: () => ({}),
};

export default App;
