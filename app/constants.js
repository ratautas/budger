// export const API_URL = 'http://budger.localhost/api/';
export const API_URL = 'http://localhost:1337/';
export const LOCALSTORAGE_STATE = 'appState';

export const LOGIN = `${API_URL}auth/local`;

export const DATASHEETS = `${API_URL}datasheets`;
export const ENTRIES = `${API_URL}entries`;
export const GET_USER = `${API_URL}auth/me`;

export const IMPORT = `${API_URL}import`;
