import { createSelector } from 'reselect';

export const selectNotificationRegistry = state =>
  state.get('notificationRegistry');

export const selectNotificationIsActive = createSelector(
  selectNotificationRegistry,
  notificationRegistry => notificationRegistry.get('isActive'),
);

export const selectNotificationParams = createSelector(
  selectNotificationRegistry,
  notificationRegistry => notificationRegistry.get('params'),
);
