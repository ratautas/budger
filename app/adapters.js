// Unwrap deep-nested erros
export function adaptResponseErrors(response) {
  const messages = response?.data?.[0]?.messages ?? [
    { id: 0, message: 'Unknown error has occured.' },
  ];
  return { ...response, messages };
}
