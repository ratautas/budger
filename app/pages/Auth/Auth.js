/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { LocalForm, Control } from 'react-redux-form';
import PropTypes from 'prop-types';

import { Button, Tabs, Tab, TextInput } from 'carbon-components-react';
import isEmail from 'validator/es/lib/isEmail';
class Auth extends Component {
  constructor(props) {
    super(props);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.attachDispatch = this.attachDispatch.bind(this);
  }

  componentDidMount() {
    // console.log('auth');
    // this.props.login('a');
    this.props.login({
      email: 'algirdas.tamasauskas@gmail.com',
      password: '8simboliai',
    });
  }

  handleFormSubmit({ email, password }) {
    this.props.login({ email, password });
  }

  attachDispatch(dispatch) {
    this.formDispatch = dispatch;
  }

  render() {
    const { userName } = this.props;
    return (
      <LocalForm
        onSubmit={this.handleFormSubmit}
        model="user"
        getDispatch={this.attachDispatch}
        initialState={{
          email: 'algirdas.tamasauskas@gmail.com',
          password: '8simboliai',
        }}
      >
        {userName}
        <Tabs
          ariaLabel="listbox"
          iconDescription="show menu options"
          // onKeyDown={() => { }}
          // onSelectionChange={() => { }}
          role="navigation"
          tabContentClassName="tab-content"
          triggerHref="#"
          type="default"
        >
          <Tab label="Login" tabIndex={0}>
            <Control.text
              autoComplete="off"
              component={TextInput}
              id="email"
              labelText="User email"
              mapProps={{
                invalid: ({ fieldValue: { valid, touched } }) =>
                  !valid && touched,
                invalidText: ({
                  fieldValue: {
                    errors: { hasValue, isEmail },
                  },
                }) => {
                  if (hasValue) return 'is required';
                  if (isEmail) return 'is not an email';
                },
              }}
              model=".email"
              placeholder="User email"
              type="email"
              validateOn="change"
              validators={{
                hasValue: val => val && val.length,
                isEmail: val => val && isEmail(val),
              }}
            />
            <Control.text
              component={TextInput}
              id="password"
              labelText="Password"
              mapProps={{
                invalid: ({ fieldValue: { valid, touched } }) =>
                  !valid && touched,
                invalidText: ({
                  fieldValue: {
                    errors: { longEnough },
                  },
                }) => {
                  if (longEnough) return 'is not long enough';
                },
              }}
              model=".password"
              placeholder="Password"
              type="password"
              validateOn="change"
              validators={{
                longEnough: val => val && val.length > 7,
              }}
            />
            <Button type="submit">Login</Button>
          </Tab>
          <Tab label="Register" tabIndex={0}>
            <Control.text
              component={TextInput}
              id="email"
              labelText="User email"
              mapProps={{
                invalid: ({ fieldValue: { valid, touched } }) =>
                  !valid && touched,
                invalidText: ({
                  fieldValue: {
                    errors: { hasValue, isEmail },
                  },
                }) => {
                  if (hasValue) return 'is required';
                  if (isEmail) return 'is not an email';
                },
              }}
              model=".email"
              placeholder="User email"
              type="email"
              validateOn="change"
              validators={{
                hasValue: val => val && val.length,
                isEmail: val => val && isEmail(val),
              }}
            />
            <Control.text
              component={TextInput}
              id="password"
              labelText="Password"
              mapProps={{
                invalid: ({ fieldValue: { valid, touched } }) =>
                  !valid && touched,
                invalidText: ({
                  fieldValue: {
                    errors: { longEnough },
                  },
                }) => {
                  if (longEnough) return 'is not long enough';
                },
              }}
              model=".password"
              placeholder="Password"
              type="password"
              validateOn="change"
              validators={{
                longEnough: val => val && val.length > 7,
              }}
            />
            <Button type="submit">Register</Button>
          </Tab>
        </Tabs>
      </LocalForm>
    );
  }
}

Auth.defaultProps = {
  userName: null,
  login: () => ({}),
};

Auth.propTypes = {
  userName: PropTypes.string,
  login: PropTypes.func,
};

export default Auth;
