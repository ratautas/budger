import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const selectAuth = state => state.get('auth') ?? Map({});

export const selectAuthUser = createSelector(
  selectAuth,
  auth => auth.get('user') ?? Map({}),
);

export const selectAuthToken = createSelector(selectAuth, auth =>
  auth.get('token'),
);

export const selectAuthLoggedIn = createSelector(selectAuth, auth =>
  auth.get('loggedIn'),
);

export const selectAuthError = createSelector(selectAuth, auth =>
  auth.get('error'),
);

export const selectUserName = createSelector(selectAuthUser, user =>
  user.get('username'),
);

export const selectDefaultDatasheetID = createSelector(selectAuthUser, user =>
  user.getIn(['default_datasheet', 'id']),
);
