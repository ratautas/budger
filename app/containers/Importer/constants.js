export const OPEN_NOTIFICATION = 'budger/Importer/OPEN_NOTIFICATION';
export const CLOSE_NOTIFICATION = 'budger/Importer/CLOSE_NOTIFICATION';

export const BANKS = [
  { label: 'SEB', value: 'seb' },
  { label: 'Revolut', value: 'revolut' },
];
